# EUNOMIA Text Similarity

## Environment variables

- TEXT_SIMILARITY_PORT=5050
- TEXT_SIMILARITY_WEB_CONCURRENCY=1 # test performance and change it if needed
- TEXT_SIMILARITY_IMAGE= # where to upload the built image
- TEXT_SIMILARITY_TAG=latest

## Usage

- Word embeddings: <br />
    To get a post's text embeddings, an HTTP POST request has to be made:
  - url: <http://eunomia-text-similarity:5050/embeddings>
  - json body: {"posts": ["text of post_1"], ... ["text of post_n"]}}
    The result is returned in json format:
    - {"result": [<embeddings_1>,...,<embeddings_n>}<br />
      where:
        <embeddings_k> (k from 1 to n) is a list of lists of floats (the embeddings for post_k)
- Similarity: <br />
    To get the text similarity results for a specific post, an HTTP POST request has to be made:
  - url: <http://eunomia-text-similarity:5050/text_similarity>
  - json body: {"similar_posts": [["text of existing post_1", "text of new post"], ...,["text of existing post_n", "text of new post"]}}
    The result of the analysis in json format is:
  - {"result": [<similarity_1>,...,<similarity_n>]}<br />
    where:
    - <similarity_k> (k from 1 to n) is a float between 0 and 1 indicating the text similarity between "post_k" and the new post

## Other

- Simple test calls and results in [./tests](./tests) <br />
- Docker compose logs, docker-stats and curl responses in [./stats](./stats)
