"""-*- coding: utf-8 -*-."""
import os
import json
import multiprocessing

bind = f"0.0.0.0:{os.environ.get('TEXT_SIMILARITY_PORT', '5050')}"
# worker_class = "uvicorn.workers.UvicornWorker"
worker_class = "uvloop"
workers_per_core_str = os.getenv("TEXT_SIMILARITY_WORKERS_PER_CORE", "1")
web_concurrency_str = os.getenv("TEXT_SIMILARITY_WEB_CONCURRENCY", None)
max_workers_str = os.getenv("TEXT_SIMILARITY_MAX_WORKERS")
max_requests_str = os.getenv("TEXT_SIMILARITY_MAX_REQUESTS", "1")
use_max_workers = None
if max_workers_str:
    use_max_workers = int(max_workers_str)
cores = multiprocessing.cpu_count()
workers_per_core = float(workers_per_core_str)
default_web_concurrency = workers_per_core * cores
if web_concurrency_str:
    web_concurrency = int(web_concurrency_str)
    if not web_concurrency > 0:
        raise RuntimeError("not web_concurrency > 0")
else:
    web_concurrency = max(int(default_web_concurrency), 2)
    if use_max_workers:
        web_concurrency = min(web_concurrency, use_max_workers)

timeout = 120
graceful_timeout = 120
keepalive = 12
workers = web_concurrency
accesslog = "-"
errorlog = "-"
max_requests = 0
try:
    max_requests = min(int(max_requests_str), 1000)
    max_requests_jitter = 1000
except (ValueError, Exception):
    pass

if max_requests < 0:
    max_requests = 0

preload_app_str = os.environ.get("TEXT_SIMILARITY_PRELOAD_APP", "true")
preload_app = preload_app_str != "false"
loglevel = "INFO" if os.environ.get("APP_ENV", "") == "production" else "DEBUG"
