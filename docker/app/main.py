"""-*- coding: utf-8 -*-."""
import os
from typing import List
from fastapi import FastAPI
from fastapi.responses import ORJSONResponse, Response
from pydantic import BaseModel
import orjson


from .paraphrase import compute_similarity
from .sentence_embeddings import get_embeddings

app = FastAPI(redoc_url=None, docs_url=None, default_response_class=ORJSONResponse)


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():  # pragma: no cover
    """Health check."""
    return None


class SimilarityRequestBody(BaseModel):
    """The list of strings to check their similarity."""

    similar_posts: List[List[str]]


class EmbeddingsRequestBody(BaseModel):
    """The list of strings to get their vectorized representation."""

    posts: List[str]


@app.post("/embeddings")
async def post_embeddings(body: EmbeddingsRequestBody):
    """Text similarity post embeddings.

    Returns the vector representation of the input
    """
    result = get_embeddings(posts=body.posts)
    data = orjson.dumps({"result": result}, option=orjson.OPT_SERIALIZE_NUMPY)
    return Response(content=data, media_type="application/json")


@app.post("/text_similarity")
async def text_similarity(body: SimilarityRequestBody):
    """Text similarity analysis post handler.

    Returns the similarities (list of floats [0,1]) between pairs of posts
    """
    result = compute_similarity(body.similar_posts)
    data = orjson.dumps({"result": result}, option=orjson.OPT_SERIALIZE_NUMPY)
    return Response(content=data, media_type="application/json")
