"""-*- coding: utf-8 -*-."""
from typing import List
from simpletransformers.classification import ClassificationModel
import os
import sys
import json

MODEL_PATH = os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..", "models", "best_roberta_stsb")
)
MODEL = "roberta"
STR_LENGTH_LIMIT = 180


class ParaphraseHandler(object):
    """Paraphrasal check class."""

    __slots__ = ["model"]

    def __init__(self) -> None:
        """Initialize handler's model."""
        self.model = ClassificationModel(
            MODEL,
            MODEL_PATH,
            num_labels=1,
            use_cuda=False,
            args={"silent": True, "use_multiprocessing": False},
        )


handler = ParaphraseHandler()


def compute_similarity(list_of_texts: List[List[str]]) -> List[float]:
    """Get the similarity between pairs of strings."""
    result = handler.model.predict([[y[:STR_LENGTH_LIMIT] for y in x] for x in list_of_texts])[0] / 5
    return result


if __name__ == "__main__":
    # example:
    # --posts '{"data": [["Hi there", "Hello there"], ["Hello here", "Hello there"], ["Hi", "Hi"]]}'
    if "--posts" in sys.argv:
        prefix_arg_index = sys.argv.index("--posts")
        try:
            if 0 <= prefix_arg_index < len(sys.argv):
                _posts = sys.argv[prefix_arg_index + 1]
                posts = json.loads(_posts).get("data", None)
                if posts:
                    print(compute_similarity(posts))
        except Exception as e:
            print(e)
