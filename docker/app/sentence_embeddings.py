"""-*- coding: utf-8 -*-."""
from typing import List
from sentence_transformers import SentenceTransformer
import torch
import os
import sys
import json

model_path = os.path.join(
    os.path.realpath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "models",
            "roberta-large-nli-stsb-mean-tokens",
        )
    )
)


class Extractor(object):
    """The embeddings extractor class."""

    __slots__ = ["model"]

    def __init__(self):
        """Handler's model initialization."""
        self.model = SentenceTransformer(
            model_name_or_path=model_path,
            device="cuda:0" if torch.cuda.is_available() else "cpu",
        )


extractor = Extractor()


def get_embeddings(posts: List[str]) -> List[List[float]]:
    """Get the vectorized version of the input's strings."""
    results = []
    for post in posts:
        tokens = extractor.model.encode([post])
        results.append(tokens)
    return results


if __name__ == "__main__":
    if "--posts" in sys.argv:
        # example:
        #  --posts '{"data": ["Hi there", "Hello there", "post to get embeddings"]}'
        prefix_arg_index = sys.argv.index("--posts")
        try:
            if 0 <= prefix_arg_index < len(sys.argv):
                __posts = sys.argv[prefix_arg_index + 1]
                _posts = json.loads(__posts).get("data", None)
                if _posts:
                    print(get_embeddings(_posts))
        except Exception as e:
            print(e)
