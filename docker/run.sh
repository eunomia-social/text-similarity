#!/usr/bin/env bash

APP_ENV="${APP_ENV:-production}"
TEXT_SIMILARITY_PORT=${TEXT_SIMILARITY_PORT:-5050}

if [ "${APP_ENV}" = "development" ];then
  LD_PRELOAD=/usr/local/lib/libjemalloc.so uvicorn app.main:app --host 0.0.0.0 --port "$TEXT_SIMILARITY_PORT" --reload
else
  # LD_PRELOAD=/usr/local/lib/libjemalloc.so gunicorn -c /app/gunicorn.py app.main:app
  LD_PRELOAD=/usr/local/lib/libjemalloc.so hypercorn --config file:/app/corn.py app.main:app
fi
