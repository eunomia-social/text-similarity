#!/bin/bash
header="Content-Type: application/json"
text1="$(curl -fsSL https://loripsum.net/api/1/long/plaintext)"
text2="$(curl -fsSL https://loripsum.net/api/1/long/plaintext)"
text3="$(curl -fsSL https://loripsum.net/api/1/medium/plaintext)"
text4="$(curl -fsSL https://loripsum.net/api/1/medium/plaintext)"
data1="{\"similar_posts\": [[\"${text1}\",\"${text2}\"],[\"${text3}\",\"${text4}\"]]}"
data2="{\"posts\": [\"${text1}\",\"${text2}\",\"${text3}\",\"${text4}\"]}"
url1="http://localhost:5050/text_similarity"
url2="http://localhost:5050/embeddings"

data=${data1}
url=${url1}
endpoint="/text_similarity"
mkdir -p output

mode=sequential
count=1024
concurrency=8

if [ "${1}" = "--embeddings" ]; then
    shift
    endpoint="/embeddings"
    data=${data2}
    url=${url2}
fi

if [ "${1}" = "--count" ]; then
    shift
    count=${1}
    shift
fi
if [ "${1}" = "--parallel" ]; then
    shift
    mode=parallel
    if [ "${1}" = "--concurrency" ]; then
    shift
    concurrency=${1}
    shift
    fi
fi
start=1
end=$count
if [ "${1}" = "--append" ] && [ -f "output/${mode}.csv" ] ; then
    start=$(cat "output/${mode}.csv" | wc -l)
    end=$((start + count))
fi
shift "$#"
if [ "${start}" = "1" ]; then
    if [ "${mode}" = "sequential" ]; then
        echo "Id,Endpoint,Total Time(s),Status Code" > "output/${mode}.csv"
    else
        echo "Id,Endpoint,Total Time(s),Status Code,Concurrency" > "output/${mode}.csv"
    fi
fi
if [ "${mode}" = "sequential" ]; then
    seq ${start} ${end} | xargs -I "$" curl -sS -w "$(date +%s)$,${endpoint},%{time_total},%{response_code}\n" -o /dev/null --request POST --url "${url}" --header "${header}" --data "${data}" >> "output/${mode}.csv"
else
    seq ${start} ${end} | xargs -I "$" -n1 -P${concurrency} curl -sS -w "$(date +%s)$,${endpoint},%{time_total},%{response_code},${concurrency}\n" -o /dev/null --request POST --url "${url}" --header "${header}" --data "${data}" >> "output/${mode}.csv"
fi

# ctr-c to stop, to view the output, on a new terminal/tab: tail -f output/{sequential|parallel}.csv
exit 0

