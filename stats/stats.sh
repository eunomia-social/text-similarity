#!/bin/bash
mkdir -p output
i=1
if [ "${1}" = "--append" ] && [ -f "output/docker_stats.csv" ] ; then
    i=$(cat output/docker_stats.csv | wc -l)
fi
shift "$#"
if [ "${i}" = "1" ]; then
    echo "Id,CPU(%),Memory(%),Memory(Usage / Limit),Net I/O,Block I/O" > output/docker_stats.csv
fi
while true; do
    docker stats --no-stream --format "${i},{{.CPUPerc}},{{.MemPerc}},{{.MemUsage}},{{.NetIO}},{{.BlockIO}}" >> output/docker_stats.csv;
    sleep 1;
    i=$((i+1))
done
# ctr-c to stop, to view, on a new terminal/tab: tail -f output/docker_stats.csv;
