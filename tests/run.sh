#!/bin/bash
text1="This is the post text to be tested"
text2="This is a post text to be tested"
text3="This is another post text to be tested"
text4="This a different post that is not that similar with the rest"

data1="{\"similar_posts\": [[\"${text1}\",\"${text2}\"],[\"${text1}\",\"${text3}\"],[\"${text1}\",\"${text4}\"]]}"
data2="{\"similar_posts\": [[\"${text2}\",\"${text3}\"],[\"${text2}\",\"${text4}\"],[\"${text2}\",\"${text1}\"]]}"
data3="{\"similar_posts\": [[\"${text3}\",\"${text4}\"],[\"${text3}\",\"${text1}\"],[\"${text3}\",\"${text2}\"]]}"
data4="{\"similar_posts\": [[\"${text4}\",\"${text1}\"],[\"${text4}\",\"${text2}\"],[\"${text4}\",\"${text3}\"]]}"

# docker-compose up -d
# sleep 20
echo "Input,Output" > test_results.csv

response1=$(curl -sS --request POST --url http://localhost:5050/text_similarity --header 'Content-Type: application/json' --data "${data1}")
csv_input1="$(echo ${data1} | sed 's/,/;/g')"
csv_output1="$(echo ${response1} | sed 's/,/;/g')"
echo "${csv_input1},${csv_output1}'" >> test_results.csv

response2=$(curl -sS --request POST --url http://localhost:5050/text_similarity --header 'Content-Type: application/json' --data "${data2}")
csv_input2="$(echo ${data2} | sed 's/,/;/g')"
csv_output2="$(echo ${response2} | sed 's/,/;/g')"
echo "${csv_input2},${csv_output2}'" >> test_results.csv

response3=$(curl -sS --request POST --url http://localhost:5050/text_similarity --header 'Content-Type: application/json' --data "${data3}")
csv_input3="$(echo ${data3} | sed 's/,/;/g')"
csv_output3="$(echo ${response3} | sed 's/,/;/g')"
echo "${csv_input3},${csv_output3}'" >> test_results.csv

response4=$(curl -sS --request POST --url http://localhost:5050/text_similarity --header 'Content-Type: application/json' --data "${data4}")
csv_input4="$(echo ${data4} | sed 's/,/;/g')"
csv_output4="$(echo ${response4} | sed 's/,/;/g')"
echo "${csv_input4},${csv_output4}" >> test_results.csv
